package com.example.listviewtest;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//on passe l'activity en tant que Context. Utile pour le LayoutInflater.
		MonAdapter adapter = new MonAdapter(this); 
		
		ListView myListView = (ListView) findViewById(R.id.theList);
		myListView.setAdapter(adapter);
		myListView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> list, View cell, int position, long id) {
		
		String ville = (String) list.getAdapter().getItem(position);
		String message = "Clic sur : " + ville + ", position: " + position;
		
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	

}
