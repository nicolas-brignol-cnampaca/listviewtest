package com.example.listviewtest;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

public class MonAdapter implements ListAdapter {

	protected Context context;
	protected List<String> cities;
	
	public MonAdapter(Context context){
		this.context = context;
		this.cities = new ArrayList<String>();
		
		//dans un vrai projet, les textes devrait se trouver dans le Strings.xml
		this.cities.add("Puyricard");
		this.cities.add("Venelles");
		this.cities.add("Tokyo");
		this.cities.add("Meyrargues");
		this.cities.add("Peyrolles");
		this.cities.add("New York");
		this.cities.add("Ensuès la Redonne");
		this.cities.add("Gémenos");
		this.cities.add("Jouques");
		this.cities.add("Aubagne");
		this.cities.add("Châteauneuf les Martigues");
		this.cities.add("Los Angeles");
		this.cities.add("Winterfell");
		this.cities.add("La Ciotat");
	}
	
	@Override
	public int getCount() {
		Log.d("Adapter", "getCount()");
		return this.cities.size();
	}

	@Override
	public Object getItem(int position) {
		Log.d("Adapter", "getItem( " + position + ")");
		return this.cities.get(position);
	}

	@Override
	public long getItemId(int position) {
		Log.d("Adapter", "getItemId( " + position + ")");
		
		//la position dans le tableau nous sert d'identifiant
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		Log.d("Adapter", "getItemViewType( " + position + ")");
		
		//un seul type de vue
		return 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("Adapter", "getView( " + position + ", " + convertView + "," + parent + ")");
		
		View cellView = convertView;

	    if (cellView == null) {
	        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        //c'est le fichier de layout "res/layout/cellule.xml" qui va etre chargé.
	        cellView = inflater.inflate(R.layout.cellule, parent, false);
	    }

	    //ces elements correspondent aux elements dans le fichier cellule.xml
	    TextView titleView = (TextView) cellView.findViewById(R.id.title);
	    TextView contentView = (TextView) cellView.findViewById(R.id.description);

	    titleView.setText(this.cities.get(position));
	    contentView.setText("position : " + position);

	    return cellView;
	}

	@Override
	public int getViewTypeCount() {
		Log.d("Adapter", "getViewTypeCount()");
		
		//un seul type de vue
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		Log.d("Adapter", "hasStableIds()");
		
		//on igore cette méthode
		return true;
	}

	@Override
	public boolean isEmpty() {
		Log.d("Adapter", "isEmpty()");
		
		if (this.cities.size() > 0) {
			return false;
		}
		
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		Log.d("Adapter", "registerDataSetObserver(" + observer + ")");
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		Log.d("Adapter", "unregisterDataSetObserver(" + observer + ")");
	}

	@Override
	public boolean areAllItemsEnabled() {
		Log.d("Adapter", "areAllItemsEnabled()");
		
		//on peut cliquer sur tous les éléments
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		Log.d("Adapter", "isEnabled( " + position + ")");
		
		//on peut cliquer sur tous les éléments
		return true;
	}

}
